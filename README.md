# Blowback
A movement-based first-person shooter built with Unreal Engine.


## Gameplay
The player must navigate the level and search for targets using their Blowback shotgun and grappling hook. Once all
targets are found and destroyed, the level ends and the game is won.

Left-click to fire your shotgun. Shooting will destroy any target that is within range and inside the reticle, indicated by the reticle color. The recoil from the blast will propel you in the opposite direction that you are aiming. Two shots can be fired in quick succession before needing to reload by pressing R.
Right-click and hold to fire the grappling hook. The reticle will indicate whether the spot you are aiming at is within grappling range. Release right-click to release the grapple. The grapple will also release when you are close to the grapple point.
