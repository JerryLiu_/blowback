// Fill out your copyright notice in the Description page of Project Settings.


#include "MovingCube.h"

// Sets default values
AMovingCube::AMovingCube()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMovingCube::BeginPlay()
{
	Super::BeginPlay();
	
	OldLocation = GetActorLocation();
	NewLocation = OldLocation + MoveDirection;
}

// Called every frame
void AMovingCube::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (AllowMovement)
	{
		FVector NewPosition = FMath::VInterpConstantTo(GetActorLocation(), NewLocation, DeltaTime, MoveTime);
		if (NewPosition == NewLocation)
		{
			Swap(NewLocation, OldLocation);
		}

		SetActorLocation(NewPosition);
	}

	if (AllowRotation)
	{
		AddActorLocalRotation(RotationSpeed * DeltaTime);
	}
}
