// Fill out your copyright notice in the Description page of Project Settings.


#include "GrappleProjectile.h"
#include "Math/UnrealMathUtility.h"

// Sets default values
AGrappleProjectile::AGrappleProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AGrappleProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGrappleProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (IsMoving)
	{
		FVector ParentLocation;
		if (GetAttachParentActor())
		{
			ParentLocation = GetAttachParentActor()->GetActorLocation();
		}
		FVector Location = FMath::VInterpConstantTo(GetActorLocation(), ParentLocation + RelativeDestinationLocation, DeltaTime, Speed);
		SetActorLocation(Location);

		if (Location == ParentLocation + RelativeDestinationLocation)
		{
			IsMoving = false;
		}
	}
}

void AGrappleProjectile::FireProjectile(FVector EndDestination, float MoveSpeed)
{
	FVector ParentLocation;
	if (GetAttachParentActor())
	{
		ParentLocation = GetAttachParentActor()->GetActorLocation();
	}
	RelativeDestinationLocation = EndDestination - ParentLocation;
	Speed = MoveSpeed;
	IsMoving = true;
}