// Fill out your copyright notice in the Description page of Project Settings.


#include "SimpleMovementComponent.h"

// Sets default values for this component's properties
USimpleMovementComponent::USimpleMovementComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void USimpleMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	OldLocation = GetOwner()->GetActorLocation();
	NewLocation = OldLocation + MoveDirection;
}


// Called every frame
void USimpleMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (AllowMovement)
	{
		FVector NewPosition = FMath::VInterpConstantTo(GetOwner()->GetActorLocation(), NewLocation, DeltaTime, MoveTime);
		if (NewPosition == NewLocation)
		{
			Swap(NewLocation, OldLocation);
		}

		GetOwner()->SetActorLocation(NewPosition);
	}

	if (AllowRotation)
	{
		GetOwner()->AddActorLocalRotation(RotationSpeed * DeltaTime);
	}
}

