// Copyright Epic Games, Inc. All Rights Reserved.

#include "BlowbackGameMode.h"
#include "BlowbackCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Target.h"

ABlowbackGameMode::ABlowbackGameMode()
	: Super()
{
	
}

void ABlowbackGameMode::BeginPlay()
{
	Super::BeginPlay();

	NumberOfTargets = GetTargetCount();
}

int32 ABlowbackGameMode::GetTargetCount()
{
	TArray<AActor*> Targets;
	UGameplayStatics::GetAllActorsOfClass(this, ATarget::StaticClass(), Targets);
	return Targets.Num();
}

void ABlowbackGameMode::TargetDestroyed()
{
	NumberOfTargets--;
	UE_LOG(LogTemp, Display, TEXT("%i"), NumberOfTargets);
}