// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BlowbackGameMode.generated.h"

UCLASS(minimalapi)
class ABlowbackGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ABlowbackGameMode();

protected:
	virtual void BeginPlay() override;

public:
	int32 NumberOfTargets;

	void TargetDestroyed();

private:
	int32 GetTargetCount();
};
