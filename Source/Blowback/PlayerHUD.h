// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PlayerHUD.generated.h"

/**
 * 
 */
UCLASS()
class BLOWBACK_API UPlayerHUD : public UUserWidget
{
	GENERATED_BODY()

private:
	UPROPERTY(meta = (BindWidget, AllowPrivateAccess = "true"))
	class UTextBlock* AmmoCounter;

	UPROPERTY(meta = (BindWidget, AllowPrivateAccess = "true"))
	class UProgressBar* ReloadBar;
};
