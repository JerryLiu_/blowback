// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "CableComponent.h"
#include "GrappleProjectile.h"
#include "Kismet/KismetMathLibrary.h"
#include "Particles/ParticleSystem.h" 	
#include "Kismet/KismetSystemLibrary.h"
#include "Target.h"

// Sets default values
APlayerCharacter::APlayerCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	GetCapsuleComponent()->InitCapsuleSize(55.f, 125.0f);

	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-10.f, 0.f, 60.f));
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	GunMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("GunMesh"));
	GunMesh->SetupAttachment(FirstPersonCameraComponent);

	CableComponent = CreateDefaultSubobject<UCableComponent>(TEXT("CableComponent"));
	CableComponent->SetupAttachment(GunMesh);
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}

	UUserWidget* HUD = CreateWidget(UGameplayStatics::GetPlayerController(this, 0), HUDClass);
	if (HUD)
	{
		HUD->AddToViewport();
	}

	CurrentAmmo = AmmoCapacity;
	IsGrappled = false;
	IsMovementKeyPressed = false;
	GetCharacterMovement()->AirControl = AirControl;
	GetCharacterMovement()->GroundFriction = GroundFriction;
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (IsGrappled)
	{
		FVector GrappleDirection = GrappleProjectile->GetActorLocation() - GetActorLocation();

		/*FVector LookDirection = Controller->GetControlRotation().Vector();
		FVector MoveDirection = GrappleDirection + LookDirection;
		MoveDirection.Normalize();*/

		GrappleDirection.Normalize();
		GetCharacterMovement()->AddForce(GrappleDirection * GetWorld()->DeltaTimeSeconds * GrappleForce);

		float GrappleDistance = FVector::Dist(GrappleProjectile->GetActorLocation(), GetActorLocation());
		if (/*IsPlayerLookingAwayFromGrapplepointAndMoving() || */GrappleDistance < GrappleDisconnectRadius)
		{
			ReleaseGrapple();
		}
	}
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(PlayerInputComponent))
	{
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Triggered, this, &ACharacter::Jump);
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &APlayerCharacter::Move);
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Started, this, &APlayerCharacter::MovementKeyPressed);
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Completed, this, &APlayerCharacter::MovementKeyReleased);
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &APlayerCharacter::Look);
		EnhancedInputComponent->BindAction(FireAction, ETriggerEvent::Triggered, this, &APlayerCharacter::Fire);
		EnhancedInputComponent->BindAction(ReloadAction, ETriggerEvent::Triggered, this, &APlayerCharacter::StartReload);
		EnhancedInputComponent->BindAction(GrappleAction, ETriggerEvent::Started, this, &APlayerCharacter::ShootGrapple);
		EnhancedInputComponent->BindAction(GrappleAction, ETriggerEvent::Completed, this, &APlayerCharacter::ReleaseGrapple);
	}
}

void APlayerCharacter::Move(const FInputActionValue& Value)
{
	FVector2D MovementVector = Value.Get<FVector2D>();

	if (Controller)
	{
		AddMovementInput(GetActorForwardVector(), MovementVector.Y);
		AddMovementInput(GetActorRightVector(), MovementVector.X);
	}
}

void APlayerCharacter::MovementKeyPressed()
{
	IsMovementKeyPressed = true;
}

void APlayerCharacter::MovementKeyReleased()
{
	IsMovementKeyPressed = false;
}

void APlayerCharacter::Look(const FInputActionValue& Value)
{
	FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (Controller)
	{
		AddControllerYawInput(LookAxisVector.X * LookSensitivity);
		AddControllerPitchInput(LookAxisVector.Y * LookSensitivity);
	}
}

void APlayerCharacter::Fire()
{
	if (Controller && CurrentAmmo > 0 && !GetWorldTimerManager().IsTimerActive(GrappleTravelTimer) && !IsGrappled)
	{
		if (IsReloading() && CurrentAmmo > 0)
		{
			GetWorldTimerManager().ClearTimer(ReloadTimer);
		}

		GunFireEvent();
		UGameplayStatics::SpawnEmitterAttached(MuzzleFlash, GunMesh, TEXT("Muzzle"));
		GetCharacterMovement()->AddImpulse(Controller->GetControlRotation().Vector() * -BlowbackForceMultiplier);
		ShootTarget();
		CurrentAmmo--;
	}
}

void APlayerCharacter::ShootTarget()
{
	FVector PlayerLocation;
	FRotator PlayerRotation;
	GetController()->GetPlayerViewPoint(PlayerLocation, PlayerRotation);
	FVector EndPoint = PlayerLocation + PlayerRotation.Vector() * ShotgunRange;

	FHitResult Hit;
	TArray<AActor*> ActorsToIgnore = TArray<AActor*>{ GetOwner() };
	UKismetSystemLibrary::SphereTraceSingle(this, PlayerLocation, EndPoint, 50.f, UEngineTypes::ConvertToTraceType(ECollisionChannel::ECC_Camera), false, ActorsToIgnore, EDrawDebugTrace::ForOneFrame, Hit, true);

	ATarget* Target = Cast<ATarget>(Hit.GetActor());
	if (Target)
	{
		Target->BreakTarget();
	}
}

void APlayerCharacter::StartReload()
{
	if (!IsReloading() && CurrentAmmo != AmmoCapacity)
	{
		GetWorldTimerManager().SetTimer(ReloadTimer, this, &APlayerCharacter::Reload, ReloadSeconds);
	}
}

void APlayerCharacter::Reload()
{
	CurrentAmmo = AmmoCapacity;
}

bool APlayerCharacter::IsReloading()
{
	return GetWorldTimerManager().IsTimerActive(ReloadTimer);
}

void APlayerCharacter::ShootGrapple()
{
	FHitResult Hit;
	if (!GetWorldTimerManager().IsTimerActive(GrappleCooldownTimer) && IsGrappleValid(Hit))
	{
		float GrappleTravelTime = Hit.Distance / GrappleSpeed;
		FVector HitLocation = Hit.Location;
		FTimerDelegate GrappleDelegate = FTimerDelegate::CreateUObject(this, &APlayerCharacter::LatchGrapple);
		GetWorldTimerManager().SetTimer(GrappleTravelTimer, GrappleDelegate, GrappleTravelTime, false);

		GrappleProjectile = SpawnGrappleProjectile();
		GrappleProjectile->AttachToActor(Hit.GetActor(), FAttachmentTransformRules::KeepWorldTransform);

		GrappleProjectile->FireProjectile(HitLocation, GrappleSpeed);
	}
}

bool APlayerCharacter::IsGrappleValid(FHitResult& OutHit)
{
	FVector PlayerLocation;
	FRotator PlayerRotation;
	GetController()->GetPlayerViewPoint(PlayerLocation, PlayerRotation);
	FVector EndPoint = PlayerLocation + PlayerRotation.Vector() * GrappleLength;

	return GetWorld()->LineTraceSingleByChannel(OutHit, PlayerLocation, EndPoint, ECollisionChannel::ECC_GameTraceChannel2); // Trace channel: 'Grapple'
}

//bool APlayerCharacter::IsPlayerLookingAwayFromGrapplepointAndMoving()
//{
//	FVector PlayerLocation;
//	FRotator PlayerRotation;
//	GetController()->GetPlayerViewPoint(PlayerLocation, PlayerRotation);
//	FVector PlayerLookDirection = PlayerRotation.Vector().GetSafeNormal();
//	FVector GrappleDirection = UKismetMathLibrary::FindLookAtRotation(PlayerLocation, GrappleLocation).Vector().GetSafeNormal();
//	float AngleInDegrees = FMath::RadiansToDegrees(FMath::Acos(PlayerLookDirection.Dot(GrappleDirection)));
//
//	if (AngleInDegrees > GrappleDisconnectAngleInDegrees && IsMovementKeyPressed)
//	{
//		return true;
//	}
//	else
//	{
//		return false;
//	}
//}

AGrappleProjectile* APlayerCharacter::SpawnGrappleProjectile()
{
	FVector MuzzleSocketLocation = GunMesh->GetSocketLocation(FName(TEXT("Muzzle")));
	AActor* Projectile= GetWorld()->SpawnActor<AActor>(GrappleProjectileBlueprint, FTransform(MuzzleSocketLocation));
	CableComponent->SetAttachEndTo(Projectile, FName(TEXT("StaticMesh")));

	return Cast<AGrappleProjectile>(Projectile);
}

void APlayerCharacter::LatchGrapple()
{
	GetCharacterMovement()->Velocity = GetCharacterMovement()->Velocity / GrappleMomentumDampingFactor;

	GetCharacterMovement()->AirControl = GrappleAirControl;
	GetCharacterMovement()->GroundFriction = 0.f;
	GetCharacterMovement()->GravityScale = GrappleGravityScale;
	IsGrappled = true;
}

void APlayerCharacter::ReleaseGrapple()
{
	if (GetWorldTimerManager().IsTimerActive(GrappleTravelTimer))
	{
		GetWorldTimerManager().ClearTimer(GrappleTravelTimer);
	}

	if (GrappleProjectile)
	{
		GrappleProjectile->Destroy();
	}

	if (IsGrappled)
	{
		FTimerDelegate EmptyDelegate;
		GetWorldTimerManager().SetTimer(GrappleCooldownTimer, GrappleCooldownSeconds, false);
	}

	GetCharacterMovement()->AirControl = AirControl;
	GetCharacterMovement()->GroundFriction = GroundFriction;
	GetCharacterMovement()->GravityScale = RegularGravityScale;
	IsGrappled = false;
}
